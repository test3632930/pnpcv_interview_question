# Pnpcv interview question
Instead of getting random questions from leetcode, we decided to simulate an existing usecase to test if candidates have the competence of reading API spec and researching.

Please use `package.json` as your base dependency file, feel free to add other packages with which you already familair.

### Goal
Create a SPA with **2** Tabs from (https://react-bootstrap.github.io/docs/components/tabs) with the name "Jobs" and "Video" that also support **RWD** (both PC and Tablet) and a forward proxy server to fetch APIs.


- Forward proxy server:
  - To let your frontend page to fetch data via a proxy server instead of getting data from our server.  
  Python with framework `FastAPI` or `Flask`, and module `requests` are preferable.
- Tab Jobs:

    - fetch POST `http://118.233.93.133:8890/api/auth/login` to get your user auth token. You will need that for the rest of tasks. You can access config.json to get username and password. Fetching format please check: https://app.cvat.ai/api/docs/#tag/auth/operation/auth_create_login

    - fetch GET `http://118.233.93.133:8890/api/jobs?org=orgForTest` to get all jobs. **Remember, you need to sepcify your Auth token in header** 
    i.e. { "Authorization": "Token _your Auth token_" } replace _your Auth token_ with an actual token, note that the word 'Token' **must** be there. Fetching format please check: https://app.cvat.ai/api/docs/#tag/jobs/operation/jobs_list
    
    - While fetching the data, if the request has not yet returned, use (https://github.com/davidhu2000/react-spinners) to improve UX.

    - Display fields "id", "updated_date"(in *YYYY mm-dd HH:MM:SS* format) with (https://react-bootstrap.github.io/docs/components/table)
    
    - Using (https://react-bootstrap.github.io/docs/components/pagination) to seperate data into **3** pages, each page contains **7** data
    
    - Example: ![Job tab and loading spinner](https://gitlab.com/test3632930/pnpcv_interview_question/-/blob/main/interview_example-tabs_and_loading_circle_-_Trim.gif), **please ignore other fields, you only need "id" and "updated_time" fields from jobs API endpoint**
    
- Tab Video(*optional, bonus question*):
  - A video player that can play video stream from `rtsp://118.233.93.133:8888/test`


### Submit your work
We'll send you an invitation to become a member of this project.
When you submit your work, please create a new branch with the name of your gitlab username.

### Problems
If you encounter any problem during the process(URL not working, unclear with the goal, .etc...), please create an issue.